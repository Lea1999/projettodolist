<?php

namespace App\Controller;

use App\Entity\Todolist;
use App\Form\Todolist1Type;
use App\Repository\TodolistRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Exception;

#[Route('/todolist')]
class TodolistController extends AbstractController
{
    #[Route('/', name: 'todolist_index', methods: ['GET'])]
    public function index(TodolistRepository $todolistRepository): Response
    {
        return $this->render('todolist/index.html.twig', [
            'todolists' => $todolistRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'todolist_new', methods: ['GET', 'POST'])]
    public function CreateTodolist(Request $request, EntityManagerInterface $entityManager): Response
    {
        $todolist = new Todolist();
        $user = new User('JeanDelacuillere@gmail.com', 'Jean', 'Delacuillère', '2000', 'password');
        $form = $this->createForm(Todolist1Type::class, $todolist);
        $form->handleRequest($request);

        if(empty($user->getTodolist())) {
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager->persist($todolist);
                $entityManager->flush();

                return $this->redirectToRoute('todolist_index', [], Response::HTTP_SEE_OTHER);
                // return true;
            }
        } else {
            throw new Exception('Already exist');
        }
        
        return $this->renderForm('todolist/new.html.twig', [
            'todolist' => $todolist,
            'form' => $form,
        ]);
    }
}
