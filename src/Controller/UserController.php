<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Carbon\Carbon;

class UserController extends AbstractController
{
    #[Route('/user', name: 'user')]
    public function index(): Response
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    public function isValid(User $user) {
        $birthdate = Carbon::parse($user->getBirthdate());
        return !empty($user->getEmail())
            && filter_var($user->getEmail(), FILTER_VALIDATE_EMAIL)
            && !empty($user->getFirstname())
            && !empty($user->getLastname())
            && strlen($user->getPassword() >= 8)
            && strlen($user->getPassword() <= 40)
            && !is_null($user->getBirthdate())
            && $birthdate->addYears(13)->isBefore(Carbon::now());
    }
}
