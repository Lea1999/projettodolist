# ProjetToDoList

### I - Présentation du projet

Pour ce projet nous avons choisi d’utiliser le framework symfony.

Nos fonctions se trouvent donc dans le controller, les entités représentent le modèle de la base de données que nous avons créé.

Les tests se trouvent dans le dossier 'test' à la racine du projet.

Nous avons créé :

- une fonction pour vérifier les informations de l’utilisateur

- une fonction permettant de créer une seule todolist par utilisateur

- une fonction pour créer des items dans une todolist avec un temps d’attente de 30 mn entre chaque ajout d’item

Pour la partie test, nous avons créé :

- une fonction qui teste le bon fonctionnement de la fonction createTodolist

- une fonction qui teste la méthode isValid() pour les utilisateurs

### II - Lancer les tests unitaires

Pour lancer les tests unitaires du projet, utilisez cette commande :

```
php ./vendor/bin/phpUnit
```
