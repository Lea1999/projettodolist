<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private User $user;

    protected function setUp(): void
    {
        $this->user = new User("jonh@mail.fr", "John", "Connor", "1999", "azertyuiop");
        parent::setUp();
    }

    public function testUserIsValidNominal()
    {   
        $this->assertTrue($this->user->isValid());
    }

    public function testUserNotValidDueToFirstname()
    {
        
        $this->user->setFirstname("");
        $this->assertFalse($this->user->isValid());
    }

    public function testNotValidDueToLastName()
    {
        $this->user->setLastname('');
        $this->assertFalse($this->user->isValid());
    }
 
    public function testNotValidDueToBirthdayInFuture()
    {
        $this->user->setBirthdate(Carbon::now()->addDecade());
        $this->assertFalse($this->user->isValid());
    }

    public function testNotValidDueToTooYoungUser()
    {
        $this->user->setBirthdate(Carbon::now()->subDecade());
        $this->assertFalse($this->user->isValid());
    }

    public function testNotValidBadEmail()
    {
        $this->user->setEmail('jonh');
        $this->assertFalse($this->user->isValid());
    }

    public function testNotValidTooShortPassword() {
        $this->user->setPassword("aaaaaaa");
        $this->assertFalse($this->user->isValid());
    }

    public function testNotValidTooLongtPassword() {
        $this->user->setPassword("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        $this->assertFalse($this->user->isValid());
    }

    public function testNotValidEmptyPassword() {
        $this->user->setPassword("");
        $this->assertFalse($this->user->isValid());
    }
}
