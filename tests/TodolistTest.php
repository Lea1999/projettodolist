<?php

namespace App;

use PHPUnit\Framework\TestCase;
use App\Entity\Todolist;
use App\Entity\User;
use App\Controller\TodolistController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class TodolistTest extends TestCase
{

    // private User $user;

    // public function testTodolistExist()
    // {
    //     // $this->assertTrue(true);
    //     $userTodolist = new User('JeanDelacuillere@gmail.com', 'Jean', 'Delacuillère', '2000', 'password');
    //     $userTodolist->setTodolist(13);
    //     $this->assertEquals(false, $userTodolist->CreateTodolist());
    // }

    public function testTodoListAlreadyCreated()
    {

        $user = new User('JeanDelacuillere@gmail.com', 'Jean', 'Delacuillère', '2000', 'password');
        $todolist = new TodolistController();
        $user->setTodolist(13);

        $request = $this->getMockBuilder('Symfony\Component\HttpFoundation\Request')
            ->disableOriginalConstructor()
            ->setMethods(['get'])  // Nous indiquons qu'une méthode va être redéfinie.
            ->getMock();
        $request->method('get')->willReturn($request);


        // $request = $this->createMock('Symfony\Component\HttpFoundation\Request');
        $entityManager = $this->createMock('Doctrine\ORM\EntityManagerInterface');
        $todolist->CreateTodolist($request, $entityManager);
        // $result = TodolistController::CreateTodolist('POST', );
        $this->expectExceptionMessage("Already exist");
        // $this->assertFalse(false, $result);
    }
}
